$(function () {
    $('.nav-link').mouseover(function () {
        $(this).addClass('active');
    })
});

$(function () {
    $('.nav-link').mouseout(function () {
        $(this).removeClass('active');
    })
});

$(document).ready(function() {
    $('a').on('click', function() { // Au clic sur un lien/une ancre
        var page = $(this).attr('href'); // Page cible
        var speed = 750; // Durée de l'animation (en ms)
        $('html, body').animate( { scrollTop: $(page).offset().top }, speed ); // Go
        return false;
    });
});
